#include <iostream>
#include <fstream>
#include <pqxx/pqxx>

using namespace std;

#include "dbfReader.h"

struct employee_record {
    /*
        id serial NOT NULL,
        psno character varying(16) NOT NULL,
        sss_no character varying(16) NOT NULL,
        last_name character varying(128) NOT NULL,
        first_name character varying(128) NOT NULL,
        middle_name character varying(128) NOT NULL,
        nickname character varying(128) NOT NULL,
        section character varying(64) NOT NULL,
        "position" character varying(128) NOT NULL,
        "group" character varying(128) NOT NULL,
        app_date date,
        out_date date,
        CONSTRAINT employee_pkey PRIMARY KEY (id),
        CONSTRAINT employee_psno_key UNIQUE (psno)
     */

    int id;
    string psno;
    string sss_no;
    string last_name;
    string first_name;
    string middle_name;
    string nickname;
    string section;
    string position;
    string group;
    string app_date;
    string out_date;
};

int sync(pqxx::work &txn, map<string, employee_record> &m, employee_record ord);
void generate_employee_map(pqxx::work &txn, map<string, employee_record> &m);
string isoDate(string date);
string formatEmp(employee_record& emp);

int main(int argc, char** argv) {

    if (argc != 3) {
        cout << "Usage: empsync [db.conf] [gprsmast.dbf file]" << endl;
        return 1;
    }

    // Parameters
    // 1 - configuration filename
    // 2 - gprsmast.DBF location
    string dbconf(argv[1]);
    string dbffile(argv[2]);

    // Get dbstring from 1st parameter
    ifstream dbconfin;
    dbconfin.open(dbconf);
    string dbstring;
    getline(dbconfin, dbstring);

    try {
        // Database connection & transaction
        pqxx::connection c(dbstring);
        pqxx::work txn(c);

        // Maps and sets
        map<string, employee_record> empMap;

        generate_employee_map(txn, empMap);

        // Stats
        int total = 0;
        int pass = 0;

        int total_pre = empMap.size();
        int update = 0;
        int insert = 0;
        int del = 0;
        int total_post = 0;

        // Prepare for FoxPro DBF reading...
        dbfReader reader;
        reader.open(dbffile);
        reader.setIconv("WINDOWS-1252");

        // Find field indices
        int psnoIdx = reader.getFieldIndex("psno");
        int sss_noIdx = reader.getFieldIndex("sss_no");
        int last_nameIdx = reader.getFieldIndex("lname");
        int first_nameIdx = reader.getFieldIndex("fname");
        int middle_nameIdx = reader.getFieldIndex("mname");
        int nicknameIdx = reader.getFieldIndex("nickname");
        int sectionIdx = reader.getFieldIndex("section");
        int positionIdx = reader.getFieldIndex("position");
        int app_dateIdx = reader.getFieldIndex("app_date");
        int out_dateIdx = reader.getFieldIndex("out_date");

        // Field variables

        string psno;
        string sss_no;
        string last_name;
        string first_name;
        string middle_name;
        string nickname;
        string section;
        string position;
        string app_date;
        string out_date;
        /*
            id serial NOT NULL,
            psno character varying(16) NOT NULL,
            sss_no character varying(16) NOT NULL,
            last_name character varying(128) NOT NULL,
            first_name character varying(128) NOT NULL,
            middle_name character varying(128) NOT NULL,
            nickname character varying(128) NOT NULL,
            section character varying(64) NOT NULL,
            "position" character varying(128) NOT NULL,
            "group" character varying(128) NOT NULL,
            app_date date,
            out_date date,
            CONSTRAINT employee_pkey PRIMARY KEY (id),
            CONSTRAINT employee_psno_key UNIQUE (psno)
         */

        // SQL prepared statements
        c.prepare("add", "INSERT INTO hr.employee (psno, sss_no, last_name, first_name, middle_name, nickname, section, \"position\", app_date, out_date) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)");

        c.prepare("update_sss_no", "UPDATE hr.employee SET sss_no=$1 WHERE id=$2");
        c.prepare("update_last_name", "UPDATE hr.employee SET last_name=$1 WHERE id=$2");
        c.prepare("update_first_name", "UPDATE hr.employee SET first_name=$1 WHERE id=$2");
        c.prepare("update_middle_name", "UPDATE hr.employee SET middle_name=$1 WHERE id=$2");
        c.prepare("update_nickname", "UPDATE hr.employee SET nickname=$1 WHERE id=$2");
        c.prepare("update_section", "UPDATE hr.employee SET section=$1 WHERE id=$2");
        c.prepare("update_position", "UPDATE hr.employee SET position=$1 WHERE id=$2");
        c.prepare("update_app_date", "UPDATE hr.employee SET app_date=$1 WHERE id=$2");
        c.prepare("update_out_date", "UPDATE hr.employee SET out_date=$1 WHERE id=$2");

        c.prepare("del", "DELETE FROM hr.employee WHERE id=$1");

        // Temporaries
        int syncState;
        employee_record tmpEmp;

        // Loop through the items in prosheet.DBF
        while (reader.next()) {

            if (reader.isClosedRow()) { // Skip closed rows
                continue;
            }

            psno = reader.getString(psnoIdx);
            sss_no = reader.getString(sss_noIdx);
            last_name = reader.getString(last_nameIdx);
            first_name = reader.getString(first_nameIdx);
            middle_name = reader.getString(middle_nameIdx);
            nickname = reader.getString(nicknameIdx);
            section = reader.getString(sectionIdx);
            position = reader.getString(positionIdx);
            app_date = reader.getString(app_dateIdx);
            out_date = reader.getString(out_dateIdx);

            tmpEmp.psno = psno;
            tmpEmp.sss_no = sss_no;
            tmpEmp.last_name = last_name;
            tmpEmp.first_name = first_name;
            tmpEmp.middle_name = middle_name;
            tmpEmp.nickname = nickname;
            tmpEmp.section = section;
            tmpEmp.position = position;
            tmpEmp.app_date = isoDate(app_date);
            tmpEmp.out_date = isoDate(out_date);

            syncState = sync(txn, empMap, tmpEmp);

            if (syncState < 0) {
                insert++;
            } else if (syncState == 0) {
                pass++;
            } else {
                update++;
            }

            total++;
        }

        // if orderMap not empty, remove from DB
        for (auto itr = empMap.begin(); itr != empMap.end(); itr++) {
            cout << " DELETE (but, not actually) " << formatEmp(itr->second) << endl;
            // txn.prepared("del", itr->second.id).exec();
            del++;
        }

        // Release empMap
        empMap.clear();

        // Commit changes made to SQL
        txn.commit();

        // Display statistics
        // total_post = total_pre + insert - del;
        total_post = total_pre + insert - 0;

        if (update == 0 && insert == 0 && del == 0) {
            cout << "Stats (Employee Synchronization)" << endl
                << " Total gprsmast  = " << total << endl;
        } else {
            cout << "Stats (Employee Synchronization)" << endl
                << " Total gprsmast  = " << total << endl
                << " Total Initial   = " << total_pre << endl
                << " Passed          = " << pass << endl
                << " Updated         = " << update << endl
                << " Inserted    (+) = " << insert << endl
                << " Deleted     (-) = " << 0 << " (" << del << ")" << endl
                << " Total Final     = " << total_post << endl;
        }

        // Close DB connection
        // c.disconnect();

    } catch (const pqxx::sql_error &e) {
        cerr << "sql_error: " << e.what() << " / " << e.query() << endl;
        return 1;
    } catch (const std::exception &e) {
        cerr << "exception: " << e.what() << endl;
        return 1;
    }

    return 0;
}

// Synchronization
// return: -1 if new insert, 0+ for number of updates (0 means found without update, ie pass)

int sync(pqxx::work &txn, map<string, employee_record> &m, employee_record emp) {
    //        c.prepare("update_sss_no", "UPDATE hr.employee SET sss_no=$1 WHERE psno=$2");
    //        c.prepare("update_last_name", "UPDATE hr.employee SET last_name=$1 WHERE psno=$2");
    //        c.prepare("update_first_name", "UPDATE hr.employee SET first_name=$1 WHERE psno=$2");
    //        c.prepare("update_middle_name", "UPDATE hr.employee SET middle_name=$1 WHERE psno=$2");
    //        c.prepare("update_nickname", "UPDATE hr.employee SET nickname=$1 WHERE psno=$2");
    //        c.prepare("update_section", "UPDATE hr.employee SET section=$1 WHERE psno=$2");
    //        c.prepare("update_position", "UPDATE hr.employee SET \"position\"=$1 WHERE psno=$2");
    //        c.prepare("update_app_date", "UPDATE hr.employee SET app_date=$1 WHERE psno=$2");
    //        c.prepare("update_out_date", "UPDATE hr.employee SET out_date=$1 WHERE psno=$2");

    auto itr = m.find(emp.psno);
    employee_record emprec;
    int rtn = 0;

    if (itr != m.end()) {
        emprec = itr->second;

        if (emprec.sss_no != emp.sss_no) {
            cout << " UPDATE " << formatEmp(itr->second) << " sss_no at " << emprec.id << " : " << emprec.sss_no << " -> " << emp.sss_no << endl;
            txn.exec_prepared("update_sss_no", emp.sss_no, emprec.id);
            rtn++;
        }

        if (emprec.last_name != emp.last_name) {
            cout << " UPDATE " << formatEmp(itr->second) << " last_name at " << emprec.id << " : " << emprec.last_name << " -> " << emp.last_name << endl;
            txn.exec_prepared("update_last_name", emp.last_name, emprec.id);
            rtn++;
        }

        if (emprec.first_name != emp.first_name) {
            cout << " UPDATE " << formatEmp(itr->second) << " first_name at " << emprec.id << " : " << emprec.first_name << " -> " << emp.first_name << endl;
            txn.exec_prepared("update_first_name", emp.first_name, emprec.id);
            rtn++;
        }

        if (emprec.middle_name != emp.middle_name) {
            cout << " UPDATE " << formatEmp(itr->second) << " middle_name at " << emprec.id << " : " << emprec.middle_name << " -> " << emp.middle_name << endl;
            txn.exec_prepared("update_middle_name", emp.middle_name, emprec.id);
            rtn++;
        }

        if (emprec.nickname != emp.nickname) {
            cout << " UPDATE " << formatEmp(itr->second) << " nickname at " << emprec.id << " : " << emprec.nickname << " -> " << emp.nickname << endl;
            txn.exec_prepared("update_nickname", emp.nickname, emprec.id);
            rtn++;
        }

        if (emprec.section != emp.section) {
            cout << " UPDATE " << formatEmp(itr->second) << " section at " << emprec.id << " : " << emprec.section << " -> " << emp.section << endl;
            txn.exec_prepared("update_section", emp.section, emprec.id);
            rtn++;
        }

        if (emprec.position != emp.position) {
            cout << " UPDATE " << formatEmp(itr->second) << " position at " << emprec.id << " : " << emprec.position << " -> " << emp.position << endl;
            txn.exec_prepared("update_position", emp.position, emprec.id);
            rtn++;
        }

        if (emp.app_date == "NULLDATE") {
            cout << " SKIPPING " << formatEmp(itr->second) << " app_date = NULLDATE" << endl;
        } else if (emprec.app_date != emp.app_date) {
            cout << " UPDATE " << formatEmp(itr->second) << " app_date at " << emprec.id << " : " << emprec.app_date << " -> " << emp.app_date << endl;
            txn.exec_prepared("update_app_date", emp.app_date, emprec.id);
            rtn++;
        }

        if (emp.out_date == "NULLDATE") {
            if (!emprec.out_date.empty()) {
                cout << " UPDATE " << formatEmp(itr->second) << " out_date at " << emprec.id << " : [" << emprec.out_date << "] -> [" << emp.out_date << "]" << endl;
                txn.exec_prepared("update_out_date", NULL, emprec.id);
                rtn++;
            }
        } else {
            if (emprec.out_date != emp.out_date) {
                cout << " UPDATE " << formatEmp(itr->second) << " out_date at " << emprec.id << " : [" << emprec.out_date << "] -> [" << emp.out_date << "]" << endl;
                txn.exec_prepared("update_out_date", emp.out_date, emprec.id);
                rtn++;
            }
        }

        m.erase(itr);
    } else {
        cout << " NOT FOUND: INSERT " << formatEmp(emp) << endl;
        if (emp.app_date == "NULLDATE" && emp.out_date == "NULLDATE")
            txn.exec_prepared("add", emp.psno, emp.sss_no, emp.last_name, emp.first_name, emp.middle_name, emp.nickname, emp.section, emp.position, NULL, NULL);
        else if (emp.app_date == "NULLDATE")
            txn.exec_prepared("add", emp.psno, emp.sss_no, emp.last_name, emp.first_name, emp.middle_name, emp.nickname, emp.section, emp.position, NULL, emp.out_date);
        else if (emp.out_date == "NULLDATE")
            txn.exec_prepared("add", emp.psno, emp.sss_no, emp.last_name, emp.first_name, emp.middle_name, emp.nickname, emp.section, emp.position, emp.app_date, NULL);
        else
            txn.exec_prepared("add", emp.psno, emp.sss_no, emp.last_name, emp.first_name, emp.middle_name, emp.nickname, emp.section, emp.position, emp.app_date, emp.out_date);

        rtn = -1;
    }

    return rtn;
}

void generate_employee_map(pqxx::work &txn, map<string, employee_record> &m) {
    pqxx::result r = txn.exec("SELECT * FROM hr.employee");

    for (auto i = 0; i != r.size(); ++i) {
        employee_record tmp;

        r[i]["id"].to(tmp.id);
        r[i]["psno"].to(tmp.psno);
        r[i]["sss_no"].to(tmp.sss_no);
        r[i]["last_name"].to(tmp.last_name);
        r[i]["first_name"].to(tmp.first_name);
        r[i]["middle_name"].to(tmp.middle_name);
        r[i]["nickname"].to(tmp.nickname);
        r[i]["section"].to(tmp.section);
        r[i]["position"].to(tmp.position);
        r[i]["group"].to(tmp.group);
        r[i]["app_date"].to(tmp.app_date);
        r[i]["out_date"].to(tmp.out_date);

        m[tmp.psno] = tmp;
    }
}

string isoDate(string date) {
    // cout << "isoDate: [" << date << "]" << endl;
    if (date.size() != 8)
        return "NULLDATE";

    return date.substr(0, 4) + "-" + date.substr(4, 2) + "-" + date.substr(6, 2);
}

string formatEmp(employee_record& emp) {
    return emp.psno + " " + emp.last_name + ", " + emp.first_name + " (" + emp.nickname + ")";
}
