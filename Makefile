all:
	clang++ -o empsync -std=c++17 -O2 -I/usr/local/include -L/usr/local/lib -liconv -lpqxx -lpq src/dbfReader.cpp src/empsync.cpp
install:
	cp empsync /storage/philstar/bin/phsystem/

clean:
	rm empsync
